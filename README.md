# Supported editors

Currently available in [Notepad++](https://notepad-plus-plus.org/) only.

# About

Salesforce is a powerful platform with incredibly powerful tools, however one of the aspects of the built-in spreadsheet-like formula engine has been bugging me since I have started working with the platform. It's formula readability. 

Although formulas are supposed to be a quick and dirty way to get things done in couple lines of code, anyone who has ever used an advanced formula knows it can get out of hand quickly. Here is a nice sample formula from [Salesforce documentation](https://help.salesforce.com/articleView?id=useful_advanced_formulas_acct_mgmt.htm):

![Sample formula from Salesforce documentation](doc/sff_sample.PNG "Salesforce Formula")

Although it's not a one-liner and you could say it's somewhat formatted, it's still far from being readable and friendly to future maintainers. Salesforce customizations are already hard to document and comprehend on system level, without the added bonus of cipher puzzles. 

So here is the idea - add some syntax highlight to it and stick to some simple formatting convention [like the one recommended by the documentation](https://help.salesforce.com/articleView?id=customize_formula_best_practices.htm). Oh, comments are also a thing in formulas, yet it's hardly mentioned anywhere!

![Salesforce formula syntax highlight dark](doc/dark_bg.PNG "Dark style")

Way better readability with identation, highliht, code folding and comments!

# Installing

Installation is really easy:
1. download the definition `.xml` file for Notepad++
2. click `Language > Define your language... > Import..` and choose the definition `.xml` file.

Once Notepad ++ is restarted it will be available under language list.
It will also be auto-applied to files with `.sff` extension.

# Roadmap

Currently available in [Notepad++](https://notepad-plus-plus.org/) only. With a hope to add more editors and add at least one more color scheme to Notepad++.

The colors for Notepad++ language definition are based on [Monokai](https://monokai.pro/) color scheme. Unfortunatelly, UDL used in Notepad++ has hard limitations on theming - one color set only.

Although it's almost readable on white background, it's suited for dark one. Might work with lightly tinted backgrounds.

![Salesforce formula syntax highlight light](doc/light_bg.PNG "Light style")

# Lincence

Copyright (c) 2020 Evaldas Micius

This work is free.

You can redistribute it and/or modify it under the terms of the WTFPL, Version 2, as published by Sam Hocevar.

See the COPYING file for more details.